/*
* Program będący implementacją algorytmu zadania drugiego z Programowania.
* @author Dawid Dominiak
* @licence MIT
*/


#include <iostream>
#include <list>
#include <cctype>

using namespace std; //Używamy przestrzeni nazw std. Nie występują w kodzie żadne kolizje nazw.

struct range { //Definicja struktury range
	char begin, end; 
};

class Lecturer { 
	private:
		string title, name, email;
		short id;
		range collegianNameRange;
	public:
		void setTitle(string title), setName(string name), setEmail(string email), setID(short id), setRange(range collegianNameRange);
		Lecturer withTitle(string title), withName(string name), withEmail(string email), withID(short id), withRange(range collegianNameRange);
		string getTitle(), getName(), getEmail();
		short getID();
		range getRange();
};

bool surnameValidator(string surname) {
	cout << !(isupper(surname[0]) != 0) << endl << endl;
	return isupper(surname[0]) == 0;
}

string collectSurname() {
	string surname;
	do
	{
		cout << "Wpisz poprawnie nazwisko (zaczyna sie wielka litera): ";
		cin >> surname;
		cout << "\n";
	} while(!isupper(surname[0]));
	return surname;
}

bool checkIfIsSavedAfterTheDeadline() {
	char answer;
	while(answer != 'T' && answer != 'N') {
		cout << "Czy jestes zapisany po terminie? <T/N>";
		cin >> answer;
		cout << "\n";
	}
	return (answer == 'T');
}

short askForLecturerID() {
	short answer;
	cout << "Podaj numer wykladowcy: ";
	cin >> answer;
	cout << "\n";
	return answer;
}

short getIDfromLecturerList(string surname, list<Lecturer> lecturerList) {
	short answer;
	for( list<Lecturer>::iterator iter = lecturerList.begin(); iter != lecturerList.end(); ++iter) {
		if(iter->getRange().begin <= surname[0] && iter->getRange().end >= surname[0]) {
			answer = iter->getID();
		}
	}
	return answer;
}

short collectGroupID(string surname, list<Lecturer> lecturerList) {
	if(checkIfIsSavedAfterTheDeadline()) {
		return askForLecturerID();
	} else {
		return getIDfromLecturerList(surname, lecturerList);
	}
} 

list<Lecturer> initializeLecturerList() {
	Lecturer asztyber, krostek, mpieniak, bputz;
	list<Lecturer> lecturerList = {
		asztyber.withID(1).withTitle("mgr inż.").withName("Anna Sztyber").withEmail("a.sztyber@mchtr.pw.edu.pl").withRange({'A', 'I'}),
		krostek.withID(2).withTitle("mgr inż.").withName("Kornel Rostek").withEmail("k.rostek@mchtr.pw.edu.pl").withRange({'J', 'M'}),
		mpieniak.withID(3).withTitle("mgr inż.").withName("Marcin Pieniak").withEmail("pieniak@mchtr.pw.edu.pl").withRange({'N', 'S'}),
		bputz.withID(4).withTitle("prof.").withName("Barbara Putz").withEmail("b.putz@okno.pw.edu.pl").withRange({'T', 'Z'})
	};
	return lecturerList;
}

Lecturer selectLecturer(short groupID, list<Lecturer> lecturerList) {
	for( list<Lecturer>::iterator iter = lecturerList.begin(); iter != lecturerList.end(); ++iter) {
		if(iter->getID() == groupID) {
			return *iter;
		}
	}
}

int main() {
	list<Lecturer> lecturerList = initializeLecturerList();
	string surname = collectSurname();
	short groupID = collectGroupID(surname, lecturerList);
	Lecturer lecturer = selectLecturer(groupID, lecturerList);
	cout << lecturer.getID() << ". " + lecturer.getTitle() + " " + lecturer.getName() << endl
		 << lecturer.getEmail() << endl;
	return 0;
}

void Lecturer::setTitle(string title) {
	this->title = title;
}

Lecturer Lecturer::withTitle(string title) {
	setTitle(title);
	return *this;
}

string Lecturer::getTitle() {
	return title;
}

void Lecturer::setName(string name) {
	this->name = name;
}

Lecturer Lecturer::withName(string name) {
	setName(name);
	return *this;
}

string Lecturer::getName() {
	return name;
}

void Lecturer::setEmail(string email) {
	this->email = email;
}

Lecturer Lecturer::withEmail(string email) {
	setEmail(email);
	return *this;
}

string Lecturer::getEmail() {
	return email;
}

void Lecturer::setID(short id) {
	this->id = id;
}

Lecturer Lecturer::withID(short id) {
	setID(id);
	return *this;
}

short Lecturer::getID() {
	return id;
}

void Lecturer::setRange(range range) {
	collegianNameRange = range;
}

Lecturer Lecturer::withRange(range range) {
	setRange(range);
	return *this;
}

range Lecturer::getRange() {
	return collegianNameRange;
}